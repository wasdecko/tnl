// SPDX-FileComment: This file is part of TNL - Template Numerical Library (https://tnl-project.org/)
// SPDX-License-Identifier: MIT

#include <TNL/Experimental/Arithmetics/FlopsCounter.h>

namespace TNL {

tnlFlopsCounter tnl_flops_counter;

}  // namespace TNL
